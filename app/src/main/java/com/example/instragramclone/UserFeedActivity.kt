package com.example.instragramclone

import android.content.Intent
import android.graphics.BitmapFactory
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.ViewGroup
import android.widget.ImageView
import com.parse.*
import kotlinx.android.synthetic.main.activity_user_feed.*

class UserFeedActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_feed)



        val username = intent.getStringExtra("username")
        readObject(username)

        title = "${username}'s photos"
    }

    private fun readObject(username: String) {
        val query = ParseQuery.getQuery<ParseObject>("Image")

        query.whereEqualTo("username", username)
        query.orderByDescending("createdAt")

        query.findInBackground { obj, e ->
            if (e == null && obj != null) {
                for (i in obj) {
                    val file = i.get("image") as ParseFile
                    file.getDataInBackground { data, e ->
                        if (e == null && data != null) {
                            val bitmap = BitmapFactory.decodeByteArray(data, 0, data.size)
                            val imageView = ImageView(this)

                            imageView.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)

                            imageView.setImageBitmap(bitmap)

                            linearLayout.addView(imageView)
                        }
                    }
                }
            }
        }
    }
}