package com.example.instragramclone

import android.content.Intent
import android.graphics.drawable.AnimationDrawable
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.parse.*
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(){
    private lateinit var animationDrawable: AnimationDrawable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (ParseUser.getCurrentUser() != null) {
            openDashboard()
        }

        animationDrawable = constraintLayout.background as AnimationDrawable
        animationDrawable.setEnterFadeDuration(5000)
        animationDrawable.setExitFadeDuration(2000)



        ParseAnalytics.trackAppOpenedInBackground(intent)
    }

    override fun onResume() {
        super.onResume()
        animationDrawable.start();
    }

    fun textViewClicked(view: View) {
        if ((view as TextView).tag == "Sign Up") {
            button.text = resources.getString(R.string.sign_up)
            hasAccountTV.text = resources.getString(R.string.already_have_an_account)
            view.text = resources.getString(R.string.log_in)
            view.tag = resources.getString(R.string.log_in)
            button.tag = resources.getString(R.string.sign_up)

        } else if (view.tag == "Log In") {
            button.text = resources.getString(R.string.log_in)
            hasAccountTV.text = resources.getString(R.string.don_t_have_an_account)
            view.text = resources.getString(R.string.sign_up)
            view.tag = resources.getString(R.string.sign_up)
            button.tag = resources.getString(R.string.log_in)
        }
    }

    fun buttonClicked(view: View) {
        if (view.tag == "Log In") {
            if (userNameET.text.isEmpty() || passwordET.text.isEmpty()) {
                Toast.makeText(this, "Username and password are required.", Toast.LENGTH_SHORT).show()
            } else {
                userLogIn(userNameET.text.toString(), passwordET.text.toString())
            }
        } else if (view.tag == "Sign Up") {
            if (userNameET.text.isNullOrEmpty() || passwordET.text.isNullOrEmpty()) {
                Toast.makeText(this, "Username and password are required.", Toast.LENGTH_SHORT).show()
            } else {
                userSignUp(userNameET.text.toString(), passwordET.text.toString())
            }
        }
    }

    private fun userSignUp(userName: String, password: String) {
        val user = ParseUser()
        user.username = userName
        user.setPassword(password)

        user.signUpInBackground { e ->
            if (e == null) {
                //Ok
                Log.i("Sign Up", "Successful")
                openDashboard()
            } else
                Toast.makeText(this, e.message, Toast.LENGTH_SHORT).show()
        }
    }

    private fun userLogIn(userName: String, password: String) {
        ParseUser.logInInBackground(userName, password) { user, e ->
            if (user != null) {
                Log.i("Log In", "Successful")
                openDashboard()
            } else
                Toast.makeText(this, e.message, Toast.LENGTH_SHORT).show()
        }
    }

    private fun openDashboard() {
        val intent = Intent(this, AllUsersActivity::class.java)
        startActivity(intent)
    }

    private fun checkCurrentUser() {
//        ParseUser.logOut()
        if (ParseUser.getCurrentUser() != null) {
            Log.i("Logged", ParseUser.getCurrentUser().username)
        } else {
            Log.i("Signed in?", "NOOOOOOOOOOO!")
        }
    }

    private fun readTable() {
        val query = ParseQuery.getQuery<ParseObject>("Score")

//        query.whereGreaterThan("score", 80)

        query.findInBackground { objects, e ->
            if (e == null) {
                if (objects!!.size > 0) {
                    for (obj in objects) {
                        obj!!.put("score", obj.getInt("score") + 20)
                        obj.saveInBackground()
                        Log.i("Username", obj.getString("username")!!)
                        Log.i("Score", obj.getInt("score").toString())
                    }
                }
            }
        }
    }






}