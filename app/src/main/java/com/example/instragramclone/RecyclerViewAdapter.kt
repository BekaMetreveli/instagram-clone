package com.example.instragramclone

import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.adapter_item.view.*

class RecyclerViewAdapter(private var adapterItems: List<String>) :
        RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.adapter_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        return holder.onBind()
    }

    override fun getItemCount(): Int {
        return adapterItems.size
    }

    fun setAdapterData(adapterItems: List<String>) {
        this.adapterItems = adapterItems
        this.notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private lateinit var item: String
        fun onBind() {
            item = adapterItems[adapterPosition]

            itemView.userNameTV.text = item
//            itemView.tv_description.text = item.description
//            Glide.with(itemView.iv_image).load(item.image).into(itemView.iv_image)

            itemView.setOnClickListener {
                val intent = Intent(itemView.context, UserFeedActivity::class.java)
                intent.putExtra("username", item)
                ContextCompat.startActivity(itemView.context, intent, null)
            }
        }
    }
}